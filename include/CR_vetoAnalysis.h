#ifndef CR_vetoAnalysis_H
#define CR_vetoAnalysis_H

#include "Analysis.h"

#include "EventBase.h"
#include "CutsCR_vetoAnalysis.h"

#include <TTreeReader.h>
#include <string>

class CR_vetoAnalysis: public Analysis {

public:
  CR_vetoAnalysis(); 
  ~CR_vetoAnalysis();

  void Initialize();
  void Execute();
  void Finalize();

protected:
  CutsCR_vetoAnalysis* m_cutsCR_vetoAnalysis;
  ConfigSvc* m_conf;
};

#endif
