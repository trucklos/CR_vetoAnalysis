#ifndef CutsCR_vetoAnalysis_H
#define CutsCR_vetoAnalysis_H

#include "EventBase.h"

class CutsCR_vetoAnalysis  {

public:
  CutsCR_vetoAnalysis(EventBase* eventBase);
  ~CutsCR_vetoAnalysis();
  bool CR_vetoAnalysisCutsOK();


private:
  
  EventBase* m_event;

};

#endif
