#include "CutsCR_vetoAnalysis.h"
#include "ConfigSvc.h"

CutsCR_vetoAnalysis::CutsCR_vetoAnalysis(EventBase* eventBase) {
  m_event = eventBase;
}

CutsCR_vetoAnalysis::~CutsCR_vetoAnalysis() {
  
}

// Function that lists all of the common cuts for this Analysis
bool CutsCR_vetoAnalysis::CR_vetoAnalysisCutsOK() {
  // List of common cuts for this analysis into one cut
  return true;
}

