#include "EventBase.h"
#include "Analysis.h"
#include "HistSvc.h"
#include "Logger.h"

#include "CutsBase.h"
#include "CutsCR_vetoAnalysis.h"
#include "ConfigSvc.h"
#include "CR_vetoAnalysis.h"

// Constructor
CR_vetoAnalysis::CR_vetoAnalysis()
  : Analysis()
{
  // List the branches required below, see full list here *:::: LINK TO SOFTWARE DOCS  ::::*
  // NOTE : if you try and use a variable in a branch that has NOT been loaded you will get a seg fault
  // there is not a error message for this currently. If you get a seg fault first thing to check 
  // is that you are including the required branch that variable lives on. 
  //
  m_event->IncludeBranch("pulsesTPCHG");
  m_event->IncludeBranch("pulsesTPC");
  m_event->IncludeBranch("pulsesSkin");
  m_event->IncludeBranch("pulseTPCLG");
  m_event->IncludeBranch("ms");
  m_event->IncludeBranch("pulsesODHG");
  //
  ////////

  m_event->Initialize();

  // Setup logging
  logging::set_program_name("CR_vetoAnalysis Analysis");
  // Logging level: error = 1, warning = 2, info = 3, debug = 4, verbose = 5
  
  m_cutsCR_vetoAnalysis = new CutsCR_vetoAnalysis(m_event);
  
  //create a config instance, this can be used to call the config variables. 
  m_conf = ConfigSvc::Instance();
}

// Destructor
CR_vetoAnalysis::~CR_vetoAnalysis() {
  delete m_cutsCR_vetoAnalysis;
}

// Called before event loop
void CR_vetoAnalysis::Initialize() {
  INFO("Initializing CR_vetoAnalysis Analysis");
}

// Called once per event
void CR_vetoAnalysis::Execute() {

  /////////////
  //// Plot TPC HG Pulses : no cuts
  /////////////
  for (int i = 0; i < (*m_event->m_tpcHGPulses)->nPulses; i++) {
    m_hists->BookFillHist("TPC/HGPulseArea_phd_All", 100, 0., 1000., (*m_event->m_tpcHGPulses)->pulseArea_phd[i]);
  }    

  //Plot for Multiple Scatters in Skin
  //Plot for skin pulse area
  if ((*m_event->m_multipleScatter)->nMultipleScatters==1)  {
      m_hists->BookFillHist("Skin/SkinPulsesMS", 100, 0., 40., (*m_event->m_skinPulses)->nPulses);
      for (int i=0; i<(*m_event->m_skinPulses)->nPulses; i++){
	m_hists->BookFillHist("Skin/SkinPulseArea", 100, 0., 7., (*m_event->m_skinPulses)->pulseArea_phd[i]);
      }
  }

  //Plot for Multiple scatters in OD
  //Plot for OD pulse area
  if ((*m_event->m_multipleScatter)->nMultipleScatters==1) {
    m_hists->BookFillHist("OD/ODPulseMS", 10, 0., 1000., (*m_event->m_odHGPulses)->nPulses);
    for (int i=0; i<(*m_event->m_odHGPulses)->nPulses; i++){
      m_hists->BookFillHist("OD/ODPulseArea", 100, 0., 5000, (*m_event->m_odHGPulses)->pulseArea_phd[i]);
    }
  }

  m_hists->BookFillHist("OD/ODPulseMSversion3", 10, 0., 10., (*m_event->m_odHGPulses)->nPulses);
  m_hists->BookFillHist("OD/ODPulseMSversion2", 150, 0., 1000., (*m_event->m_odHGPulses)->nPulses);

  //Plot of 2d histogram of area vs pulse for skin
  if ((*m_event->m_multipleScatter)->nMultipleScatters==1)  {
    for (int i=0; i<(*m_event->m_skinPulses)->nPulses; i++){
      m_hists->BookFillHist("Skin/2dHistOfAreavsPulseSkin", 100, 0., 1000., 100, 0., 100., (*m_event->m_skinPulses)->pulseArea_phd[i], (*m_event->m_skinPulses)->nPulses, 10);
    }
  }

  //version2 of 2d hist of area vs pulse for skin  
  if ((*m_event->m_multipleScatter)->nMultipleScatters==1)  {
    for (int i=0; i<(*m_event->m_skinPulses)->nPulses; i++){
      m_hists->BookFillHist("Skin/2dHistOfAreavsPulseSkinV2", 100, 0., 5., 10, 0., 37., (*m_event->m_skinPulses)->pulseArea_phd[i], (*m_event->m_skinPulses)->nPulses, 1);
    }
  }

  //Plot of 2d Histogram of area vs pulse for OD
  if ((*m_event->m_multipleScatter)->nMultipleScatters==1) {
    for (int i=0; i<(*m_event->m_odHGPulses)->nPulses; i++){
      m_hists->BookFillHist("OD/2dHistOfAreavsPulseOD", 100, 0., 300., 100, 0., 1000., (*m_event->m_odHGPulses)->pulseArea_phd[i], (*m_event->m_odHGPulses)->nPulses, 1);
    }
  }

  //v2 of 2d hist of area vs pulse for OD
  if ((*m_event->m_multipleScatter)->nMultipleScatters==1) {
    for (int i=0; i<(*m_event->m_odHGPulses)->nPulses; i++){
      m_hists->BookFillHist("OD/2dHistOfAreavsPulseODV2", 100, 0., 10., 100, 675., 875., (*m_event->m_odHGPulses)->pulseArea_phd[i], (*m_event->m_odHGPulses)->nPulses, 1);
    }
  }

  //no pulse? for skin
  if ((*m_event->m_multipleScatter)->nMultipleScatters==0)  {
    m_hists->BookFillHist("Skin/NOSkinPulsesMS", 100, 0., 40., (*m_event->m_skinPulses)->nPulses);
    for (int i=0; i<(*m_event->m_skinPulses)->nPulses; i++){
      m_hists->BookFillHist("Skin/NOSkinPulseArea", 100, 0., 7., (*m_event->m_skinPulses)->pulseArea_phd[i]);
    }
  }

  //no2dhist for skin
  //doesnt make sense
  if ((*m_event->m_multipleScatter)->nMultipleScatters==0)  {
    for (int i=0; i<(*m_event->m_skinPulses)->nPulses; i++){
      m_hists->BookFillHist("Skin/NO2dHistOfAreavsPulseSkin", 100, 0., 7000., 10, 0., 40., (*m_event->m_skinPulses)->pulseArea_phd[i], (*m_event->m_skinPulses)->nPulses, 1);
    }
  }
  
  //no2dhist for od
  //maybe
  if ((*m_event->m_multipleScatter)->nMultipleScatters==0)  {
    for (int i=0; i<(*m_event->m_odHGPulses)->nPulses; i++){
      m_hists->BookFillHist("OD/NO2dHistOfAreavsPulseOD", 100, 0., 900., 100, 0., 1000., (*m_event->m_odHGPulses)->pulseArea_phd[i], (*m_event->m_odHGPulses)->nPulses, 1);
    }
  }

  //area of 1st pulse vs 2nd pulse in od for nPulses>1
  //i hope
  if ((*m_event->m_multipleScatter)->nMultipleScatters==1)  {
    for (int i=1; i<(*m_event->m_odHGPulses)->nPulses; i++){
      m_hists->BookFillHist("OD/2dHistOfA1vsA2", 10, 0., 300., 10, 0., 300., (*m_event->m_skinPulses)->pulseArea_phd[1], (*m_event->m_skinPulses)->pulseArea_phd[2], 1);
    }
  }

  //time seperation in od when npulses>1
  //i hope
  if ((*m_event->m_multipleScatter)->nMultipleScatters==1){
    for (int i=1; i<(*m_event->m_odHGPulses)->nPulses;i++){
      m_hists->BookFillHist("OD/timebetweenpulse1vspulse2", 1000, 0., 500., (*m_event->m_odHGPulses)->peakTime_ns[i] - (*m_event->m_odHGPulses)->peakTime_ns[i-1], 1);
    }
  }









  ////////////////////////
  //comments
  //how to put float rq into for loop
  //use something else instead of pulsearea_phd? and why do we use it
  //*m_event is going to where exactly, a path?
  //only four m_event objects? as defined earlier in the macro
  //how to actually use multiple scatters rqs and diference in calling those rqs vs single scatter
  //2d histogram format makes sense but how to do it
  //why don't my rqs work?
  //ODPulses used wrong



}

// Called after event loop
void CR_vetoAnalysis::Finalize() {
  m_hists->GetHistFromMap("OD/ODPulseMS")->GetXaxis()->SetTitle("nPulsesOD");

}

