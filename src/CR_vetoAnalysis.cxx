#include "EventBase.h"
#include "Analysis.h"
#include "HistSvc.h"
#include "Logger.h"

#include "CutsBase.h"
#include "CutsCR_vetoAnalysis.h"
#include "ConfigSvc.h"
#include "CR_vetoAnalysis.h"

// Constructor
CR_vetoAnalysis::CR_vetoAnalysis()
  : Analysis()
{
  // List the branches required below, see full list here *:::: LINK TO SOFTWARE DOCS  ::::*
  // NOTE : if you try and use a variable in a branch that has NOT been loaded you will get a seg fault
  // there is not a error message for this currently. If you get a seg fault first thing to check 
  // is that you are including the required branch that variable lives on. 
  //
  m_event->IncludeBranch("pulsesTPCHG");
  m_event->IncludeBranch("pulsesTPC");
  m_event->IncludeBranch("pulsesSkin");
  m_event->IncludeBranch("pulseTPCLG");
  m_event->IncludeBranch("ms");
  m_event->IncludeBranch("pulsesODHG");
  m_event->IncludeBranch("other");
  m_event->IncludeBranch("ss");
  m_event->IncludeBranch("pileUp");
  //
  ////////

  m_event->Initialize();

  // Setup logging
  logging::set_program_name("CR_vetoAnalysis Analysis");
  // Logging level: error = 1, warning = 2, info = 3, debug = 4, verbose = 5
  
  m_cutsCR_vetoAnalysis = new CutsCR_vetoAnalysis(m_event);
  
  //create a config instance, this can be used to call the config variables. 
  m_conf = ConfigSvc::Instance();
}

// Destructor
CR_vetoAnalysis::~CR_vetoAnalysis() {
  delete m_cutsCR_vetoAnalysis;
}

// Called before event loop
void CR_vetoAnalysis::Initialize() {
  INFO("Initializing CR_vetoAnalysis Analysis");
}

// Called once per event
void CR_vetoAnalysis::Execute() {
  
  /////////////
  //// Plot TPC HG Pulses : no cuts
  /////////////
  for (int i = 0; i < (*m_event->m_tpcHGPulses)->nPulses; i++) {
    m_hists->BookFillHist("TPC/HGPulseArea_phd_All", 100, 0., 1000., (*m_event->m_tpcHGPulses)->pulseArea_phd[i]);
  }    

  //Plot for Multiple Scatters in Skin
  //Plot for skin pulse area
  if ((*m_event->m_multipleScatter)->nMultipleScatters==1)  {
      m_hists->BookFillHist("Skin/SkinPulsesMS", 100, 0., 40., (*m_event->m_skinPulses)->nPulses);
      for (int i=0; i<(*m_event->m_skinPulses)->nPulses; i++){
	m_hists->BookFillHist("Skin/SkinPulseArea", 100, 0., 7., (*m_event->m_skinPulses)->pulseArea_phd[i]);
      }
  }

  //Plot for Multiple scatters in OD
  //Plot for OD pulse area
  if ((*m_event->m_multipleScatter)->nMultipleScatters==1) {
    m_hists->BookFillHist("OD/ODPulseMS", 100, 0., 1000., (*m_event->m_odHGPulses)->nPulses);
    for (int i=0; i<(*m_event->m_odHGPulses)->nPulses; i++){
      m_hists->BookFillHist("OD/ODPulseArea", 1000, 0., 10, (*m_event->m_odHGPulses)->pulseArea_phd[i]);
    }
  }

  m_hists->BookFillHist("OD/ODPulseMSversion3", 10, 0., 10., (*m_event->m_odHGPulses)->nPulses);
  m_hists->BookFillHist("OD/ODPulseMSversion2", 150, 0., 1000., (*m_event->m_odHGPulses)->nPulses);
  //  if((*m_event->m_odHGPulses)->nPulses>600) std::cout << (*m_event->m_eventHeader)->rawFileName << "\t" << (*m_event->m_eventHeader)->runID << "\t" << (*m_event->m_eventHeader)->eventID << std::endl;

  
  //Plot of 2d histogram of area vs pulse for skin
  if ((*m_event->m_multipleScatter)->nMultipleScatters==1)  {
    for (int i=0; i<(*m_event->m_skinPulses)->nPulses; i++){
      m_hists->BookFillHist("Skin/2dHistOfAreavsPulseSkin", 100, 0., 1000., 100, 0., 100., (*m_event->m_skinPulses)->pulseArea_phd[i], (*m_event->m_skinPulses)->nPulses, 10);
    }
  }


  //Plot of 2d Histogram of area vs pulse for OD
  if ((*m_event->m_multipleScatter)->nMultipleScatters==1) {
    for (int i=0; i<(*m_event->m_odHGPulses)->nPulses; i++){
      m_hists->BookFillHist("OD/2dHistOfAreavsPulseOD", 100, 0., 300., 100, 0., 1000., (*m_event->m_odHGPulses)->pulseArea_phd[i], (*m_event->m_odHGPulses)->nPulses, 1);
    }
  }


  //no pulse, for skin
  //includes things like single scatter or no scatters
  if ((*m_event->m_multipleScatter)->nMultipleScatters==0)  {
    m_hists->BookFillHist("Skin/NOSkinPulsesMS", 100, 0., 40., (*m_event->m_skinPulses)->nPulses);
    for (int i=0; i<(*m_event->m_skinPulses)->nPulses; i++){
      m_hists->BookFillHist("Skin/NOSkinPulseArea", 100, 0., 7., (*m_event->m_skinPulses)->pulseArea_phd[i]);
    }
  }


  //no pulse, od
  if ((*m_event->m_multipleScatter)->nMultipleScatters==0) {
    m_hists->BookFillHist("OD/NOMSPulseOD", 100, 0., 1000., (*m_event->m_odHGPulses)->nPulses);
    for (int i=0; i<(*m_event->m_odHGPulses)->nPulses; i++){
      m_hists->BookFillHist("OD/NOPulseAreaOD", 1000, 0., 10, (*m_event->m_odHGPulses)->pulseArea_phd[i]);
    }
  }

  //no2dhist for skin
  //areavspulse of things like single scatter or no pulse
  if ((*m_event->m_multipleScatter)->nMultipleScatters==0)  {
    for (int i=0; i<(*m_event->m_skinPulses)->nPulses; i++){
      m_hists->BookFillHist("Skin/NO2dHistOfAreavsPulseSkin", 100, 0., 7000., 10, 0., 40., (*m_event->m_skinPulses)->pulseArea_phd[i], (*m_event->m_skinPulses)->nPulses, 1);
    }
  }
  
  //no2dhist for od
  if ((*m_event->m_multipleScatter)->nMultipleScatters==0)  {
    for (int i=0; i<(*m_event->m_odHGPulses)->nPulses; i++){
      m_hists->BookFillHist("OD/NO2dHistOfAreavsPulseOD", 100, 0., 900., 100, 0., 1000., (*m_event->m_odHGPulses)->pulseArea_phd[i], (*m_event->m_odHGPulses)->nPulses, 1);
    }
  }

  //area of 1st pulse vs 2nd pulse in od for nPulses>1
  if ((*m_event->m_multipleScatter)->nMultipleScatters==1)  {
    for (int i=1; i<(*m_event->m_odHGPulses)->nPulses; i++){
      m_hists->BookFillHist("OD/2dHistOfA1vsA2", 10, 0., 300., 10, 0., 300., (*m_event->m_odHGPulses)->pulseArea_phd[1], (*m_event->m_odHGPulses)->pulseArea_phd[2], 1);
    }
  }

  //version 2 of A1vsA2
  if ((*m_event->m_multipleScatter)->nMultipleScatters==1)  {
    for (int i=1; i<(*m_event->m_odHGPulses)->nPulses; i++){
      m_hists->BookFillHist("OD/V22dHistOfA1vsA2", 100, 0., 2., 100, 0., 2., (*m_event->m_odHGPulses)->pulseArea_phd[1], (*m_event->m_odHGPulses)->pulseArea_phd[2], 1);
    }
  }


  //time seperation in od when npulses>1
  if ((*m_event->m_multipleScatter)->nMultipleScatters==1){
    for (int i=1; i<(*m_event->m_odHGPulses)->nPulses;i++){
      m_hists->BookFillHist("OD/TimeBetweenPulse1vsPulse2", 100, 0., 500., (*m_event->m_odHGPulses)->peakTime_ns[i] - (*m_event->m_odHGPulses)->peakTime_ns[i-1], 1);
    }
  }






  //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------







  
  //multiple Scatter, nPulses, coincidence>345, OD
  if((*m_event->m_multipleScatter)->nMultipleScatters==1){
    for(int i=0; i <(*m_event->m_odHGPulses)->nPulses;i++){
      if ((*m_event->m_odHGPulses)->maxCoincidence > 3){
        m_hists->BookFillHist("OD/new/MS_Coin>3(old)", 100, 0., 1000., (*m_event->m_odHGPulses)->nPulses);
      }
    }
  }
  if((*m_event->m_multipleScatter)->nMultipleScatters==1){
    for(int i=0; i<(*m_event->m_odHGPulses)->nPulses;i++){
      if((*m_event->m_odHGPulses)->coincidence[i]>3){
        m_hists->BookFillHist("OD/new/MS_Coin>3", 10, 0., 20., (*m_event->m_odHGPulses)->nPulses);
      }
    }
  }
  if((*m_event->m_multipleScatter)->nMultipleScatters==1){
    for(int i=0; i<(*m_event->m_odHGPulses)->nPulses;i++){
      if((*m_event->m_odHGPulses)->coincidence[i]>4){
        m_hists->BookFillHist("OD/new/MS_Coin>4", 10, 0., 20., (*m_event->m_odHGPulses)->nPulses);
      }
    }
  }
  if((*m_event->m_multipleScatter)->nMultipleScatters==1){
    for(int i=0; i<(*m_event->m_odHGPulses)->nPulses;i++){
      if((*m_event->m_odHGPulses)->coincidence[i]>5){
        m_hists->BookFillHist("OD/new/MS_Coin>5", 10, 0., 20., (*m_event->m_odHGPulses)->nPulses);
      }
    }
  }
  for(int i=0; i<(*m_event->m_odHGPulses)->nPulses;i++){
    if((*m_event->m_odHGPulses)->coincidence[i]>3){
      m_hists->BookFillHist("OD/new/Coin>3(no_ms)", 25, 0., 50., (*m_event->m_odHGPulses)->nPulses);
    }
  }
  for(int i=0;i<(*m_event->m_odHGPulses)->nPulses;i++){
    if((*m_event->m_odHGPulses)->maxCoincidence>3){
      m_hists->BookFillHist("OD/new/Test_Coin>3(no_ms)", 25, 0., 50., (*m_event->m_odHGPulses)->nPulses);
    }
  }
  if((*m_event->m_multipleScatter)->nMultipleScatters==1){
    for(int i=0; i<(*m_event->m_odHGPulses)->nPulses; i++){
      if((*m_event->m_odHGPulses)->coincidence[i]>3 && (*m_event->m_odHGPulses)->pulseArea_phd[i]>2){
	m_hists->BookFillHist("OD/new/MS_Pulse_Timing_for_Coin>3_PulseArea>2", 1000, 0., 1000., (*m_event->m_odHGPulses)->pulseStartTime_ns[i]+(*m_event->m_odHGPulses)->areaFractionTime1_ns[i]);
      }
    }
  }
  if((*m_event->m_multipleScatter)->nMultipleScatters==1){
    for(int i=0; i<(*m_event->m_odHGPulses)->nPulses; i++){
      if((*m_event->m_odHGPulses)->pulseArea_phd[i]>2.){
        m_hists->BookFillHist("OD/new/Pulse_Timing_PulseArea>2", 1000, 0., 1000., (*m_event->m_odHGPulses)->pulseStartTime_ns[i]+(*m_event->m_odHGPulses)->areaFractionTime1_ns[i]);
      }
    }
  }
  for(int i=0; i<(*m_event->m_odHGPulses)->nPulses; i++){
    if((*m_event->m_odHGPulses)->pulseArea_phd[i]>2.){
      m_hists->BookFillHist("OD/new/V2_Pulse_Timing_PulseArea>2", 1000, 0., 100000., (*m_event->m_odHGPulses)->pulseStartTime_ns[i]+(*m_event->m_odHGPulses)->areaFractionTime1_ns[i]);
    }
  }


  /*
  //cleaner code?
  for(int i=0; i<(*m_event->m_odHGPulses)->nPulses;i++){
    if((*m_event->m_odHGPulses)->coincidence[i]>3 && (*m_event->m_multipleScatter)->nMultipleScatters==1){
      m_hists->BookFillHist("OD/new/Test2_coin>3(same_as_ms_coin>3?)", 1000, 0., 1000., (*m_event->m_odHGPulses)->nPulses);
    }
  }
  */


  
  //other, nPulses, coincidence > 345, OD
  if((*m_event->m_otherScatter)->nOtherScatters==1){
    for(int i=0; i<(*m_event->m_odHGPulses)->nPulses;i++){
      if((*m_event->m_odHGPulses)->coincidence[i]>3){
        m_hists->BookFillHist("OD/new/Other_Coin>3", 25, 0., 50., (*m_event->m_odHGPulses)->nPulses);
      }
    }
  }
  if((*m_event->m_otherScatter)->nOtherScatters==1){                                                                                                                                                       for(int i=0; i<(*m_event->m_odHGPulses)->nPulses;i++){
      if((*m_event->m_odHGPulses)->coincidence[i]>4){
        m_hists->BookFillHist("OD/new/Other_Coin>4", 25, 0., 50., (*m_event->m_odHGPulses)->nPulses);
      }
    }
  }
  if((*m_event->m_otherScatter)->nOtherScatters==1){                                                                                                                                                       for(int i=0; i<(*m_event->m_odHGPulses)->nPulses;i++){
      if((*m_event->m_odHGPulses)->coincidence[i]>5){
        m_hists->BookFillHist("OD/new/Other_Coin>5", 25, 0., 50., (*m_event->m_odHGPulses)->nPulses);
      }
    }
  }
  if((*m_event->m_otherScatter)->nOtherScatters==1){
    for(int i=0; i<(*m_event->m_odHGPulses)->nPulses; i++){
      if((*m_event->m_odHGPulses)->coincidence[i]>3 && (*m_event->m_odHGPulses)->pulseArea_phd[i]>2){
        m_hists->BookFillHist("OD/new/Other_Pulse_Timing_for_Coin>3_PulseArea>2", 10, 0., 40., (*m_event->m_odHGPulses)->pulseStartTime_ns[i]+(*m_event->m_odHGPulses)->areaFractionTime1_ns[i]);
      }
    }
  }
  //rewrite n terms of 2dhist
  if((*m_event->m_otherScatter)->nOtherScatters==1){
    for(int i=0; i<(*m_event->m_odHGPulses)->nPulses; i++){
      if((*m_event->m_odHGPulses)->coincidence[i] > 5 && (*m_event->m_odHGPulses)->pulseArea_phd[i] > 4.5){
	m_hists->BookFillHist("OD/new/Other_Coin5vsPulseArea4.5", 100, 0., 200., 100, 0., 400., (*m_event->m_odHGPulses)->coincidence[i], (*m_event->m_odHGPulses)->pulseArea_phd[i], 1);
      }
    }
  }


  

  //mutliple scatter, pulsearea_phd > 2, coincidence > 3, OD
  if((*m_event->m_multipleScatter)->nMultipleScatters==1){
    for(int i=0; i <(*m_event->m_odHGPulses)->nPulses;i++){
      if ((*m_event->m_odHGPulses)->coincidence[i] > 3 && (*m_event->m_odHGPulses)->pulseArea_phd[i] > 2){
        m_hists->BookFillHist("OD/new/MS_Coin3_A2", 100, 0., 500., (*m_event->m_odHGPulses)->pulseArea_phd[i]);
      }
    }
  }
  if((*m_event->m_multipleScatter)->nMultipleScatters==1){
    for(int i=0; i <(*m_event->m_odHGPulses)->nPulses;i++){
      if ((*m_event->m_odHGPulses)->coincidence[i] > 4 && (*m_event->m_odHGPulses)->pulseArea_phd[i] > 3){
        m_hists->BookFillHist("OD/new/MS_Coin4_A3", 100, 0., 500., (*m_event->m_odHGPulses)->pulseArea_phd[i]);
      }
    }
  }
  //ms, pulsearea_phd, coincidence > 5, pulsearea > 4, OD
  if((*m_event->m_multipleScatter)->nMultipleScatters==1){
    for(int i=0; i <(*m_event->m_odHGPulses)->nPulses;i++){
      if ((*m_event->m_odHGPulses)->coincidence[i] > 5 && (*m_event->m_odHGPulses)->pulseArea_phd[i] > 4){
        m_hists->BookFillHist("OD/new/MS_Coin5_A4", 100, 0., 500., (*m_event->m_odHGPulses)->pulseArea_phd[i]);
      }
    }
  }




  //other, pulsearea_phd > 2, coincidence > 3, OD
  if((*m_event->m_otherScatter)->nOtherScatters==1){
    for(int i=0; i <(*m_event->m_odHGPulses)->nPulses;i++){
      if ((*m_event->m_odHGPulses)->coincidence[i] > 3 && (*m_event->m_odHGPulses)->pulseArea_phd[i] > 2){
        m_hists->BookFillHist("OD/new/Other_Coin3_A2", 600, 0., 1200., (*m_event->m_odHGPulses)->pulseArea_phd[i]);
      }
    }
  }
  //other, pulsearea_phd, coincidence > 5, pulsearea > 4, OD
  if((*m_event->m_otherScatter)->nOtherScatters==1){
    for(int i=0; i <(*m_event->m_odHGPulses)->nPulses;i++){
      if ((*m_event->m_odHGPulses)->coincidence[i] > 5 && (*m_event->m_odHGPulses)->pulseArea_phd[i] > 4){
        m_hists->BookFillHist("OD/new/Other_Coin5_A4", 600, 0., 1200., (*m_event->m_odHGPulses)->pulseArea_phd[i]);
      }
    }
  }
  //other, pulsearea_phd, coincidence > 4, pulsearea > 3, OD
  if((*m_event->m_otherScatter)->nOtherScatters==1){
    for(int i=0; i <(*m_event->m_odHGPulses)->nPulses;i++){
      if ((*m_event->m_odHGPulses)->coincidence[i] > 4 && (*m_event->m_odHGPulses)->pulseArea_phd[i] > 3){
        m_hists->BookFillHist("OD/new/Other_Coin4_A3", 600, 0., 1200., (*m_event->m_odHGPulses)->pulseArea_phd[i]);
      }
    }
  }


  /*
  //ms, nPulses, coincidence > 3, skin
  if((*m_event->m_multipleScatter)->nMultipleScatters==1 && (*m_event->m_skinPulses)->maxCoincidence > 3){
    for(int i=0; i <(*m_event->m_skinPulses)->nPulses;i++){
      m_hists->BookFillHist("Skin/new/maxCoincidence3", 100, 0., 100., (*m_event->m_skinPulses)->nPulses);
    }
  }

  //other, nPulses, coincidence > 3, skin
  if((*m_event->m_otherScatter)->nOtherScatters==1 && (*m_event->m_skinPulses)->maxCoincidence > 3){
    for(int i=0; i <(*m_event->m_skinPulses)->nPulses;i++){
      m_hists->BookFillHist("Skin/new/othermaxCoincidence3", 100, 0., 1000., (*m_event->m_skinPulses)->nPulses);
    }
  }

  //ms, coincidence > 3, pulsearea > 2.5, skin
  if((*m_event->m_multipleScatter)->nMultipleScatters==1){                                                                                                                                                 for(int i=0; i <(*m_event->m_skinPulses)->nPulses;i++){
      if ((*m_event->m_skinPulses)->maxCoincidence > 3 && (*m_event->m_skinPulses)->pulseArea_phd[i] > 2.5){
        m_hists->BookFillHist("Skin/new/maxCoincidence3_pulsearea2.5", 1000, 0., 10000., (*m_event->m_skinPulses)->pulseArea_phd[i]);
      }
    }
  }

  //other, coincidence > 3, pulsearea > 2.5, skin
  if((*m_event->m_multipleScatter)->nMultipleScatters==1){                                                                                                                                            
    for(int i=0; i <(*m_event->m_skinPulses)->nPulses;i++){
      if ((*m_event->m_skinPulses)->maxCoincidence > 3 && (*m_event->m_skinPulses)->pulseArea_phd[i] > 2.5){
        m_hists->BookFillHist("Skin/new/maxCoincidence3_pulsearea2.5", 1000, 0., 10000., (*m_event->m_skinPulses)->pulseArea_phd[i]);
      }
    }
  }
*/





  //OD,coin>5,pulseArea>4.5
  if((*m_event->m_otherScatter)->nOtherScatters==1){
    for(int i=0; i<(*m_event->m_odHGPulses)->nPulses; i++){
      if((*m_event->m_odHGPulses)->coincidence[i] > 5 && (*m_event->m_odHGPulses)->pulseArea_phd[i] > 4.5){
        m_hists->BookFillHist("New/OD_Other_Coin5vsA4.5", 40, 0., 40., 30, 0., 30., (*m_event->m_odHGPulses)->pulseArea_phd[i], (*m_event->m_odHGPulses)->coincidence[i], 1);
      }
    }
  }
  if((*m_event->m_singleScatter)->nSingleScatters==1){
    for(int i=0; i<(*m_event->m_odHGPulses)->nPulses; i++){
      if((*m_event->m_odHGPulses)->coincidence[i] > 5 && (*m_event->m_odHGPulses)->pulseArea_phd[i] > 4.5){
        m_hists->BookFillHist("New/OD_SS_Coin5vsA4.5", 50, 0., 100., 75, 0., 150., (*m_event->m_odHGPulses)->pulseArea_phd[i], (*m_event->m_odHGPulses)->coincidence[i], 1);
      }
    }
  }
  if((*m_event->m_pileupScatter)->nPileUpScatters==1){
    for(int i=0; i<(*m_event->m_odHGPulses)->nPulses; i++){
      if((*m_event->m_odHGPulses)->coincidence[i] > 5 && (*m_event->m_odHGPulses)->pulseArea_phd[i] > 4.5){
        m_hists->BookFillHist("New/OD_PileUp_Coin5vsA4.5", 100, 0., 1000., 75, 0., 150., (*m_event->m_odHGPulses)->pulseArea_phd[i], (*m_event->m_odHGPulses)->coincidence[i], 1);
      }
    }
  }
  if((*m_event->m_multipleScatter)->nMultipleScatters==1){
    for(int i=0; i<(*m_event->m_odHGPulses)->nPulses; i++){
      if((*m_event->m_odHGPulses)->coincidence[i] > 5 && (*m_event->m_odHGPulses)->pulseArea_phd[i] > 4.5){
        m_hists->BookFillHist("New/OD_MS_Coin5vsA4.5", 100, 0., 500., 75, 0., 150., (*m_event->m_odHGPulses)->pulseArea_phd[i], (*m_event->m_odHGPulses)->coincidence[i], 1);
      }
    }
  }



  //Skin,coin>2,pulseArea>2.5
  if((*m_event->m_otherScatter)->nOtherScatters==1){
    for(int i=0; i<(*m_event->m_skinPulses)->nPulses; i++){
      if((*m_event->m_skinPulses)->coincidence[i] > 2 && (*m_event->m_skinPulses)->pulseArea_phd[i] > 2.5){
        m_hists->BookFillHist("New/Skin_Other_Coin2vsA2.5", 40, 0., 40., 30, 0., 30., (*m_event->m_skinPulses)->pulseArea_phd[i], (*m_event->m_skinPulses)->coincidence[i], 1);
      }
    }
  }
  if((*m_event->m_singleScatter)->nSingleScatters==1){
    for(int i=0; i<(*m_event->m_skinPulses)->nPulses; i++){
      if((*m_event->m_skinPulses)->coincidence[i] > 2 && (*m_event->m_skinPulses)->pulseArea_phd[i] > 2.5){
        m_hists->BookFillHist("New/Skin_SS_Coin2vsA2.5", 1000, 0., 3000., 75, 0., 150., (*m_event->m_skinPulses)->pulseArea_phd[i], (*m_event->m_skinPulses)->coincidence[i], 1);
      }
    }
  }
  if((*m_event->m_pileupScatter)->nPileUpScatters==1){
    for(int i=0; i<(*m_event->m_skinPulses)->nPulses; i++){
      if((*m_event->m_skinPulses)->coincidence[i] > 2 && (*m_event->m_skinPulses)->pulseArea_phd[i] > 2.5){
        m_hists->BookFillHist("New/Skin_PileUp_Coin2vsA2.5", 100, 0., 1000., 75, 0., 150., (*m_event->m_skinPulses)->pulseArea_phd[i], (*m_event->m_skinPulses)->coincidence[i], 1);
      }
    }
  }
  if((*m_event->m_multipleScatter)->nMultipleScatters==1){
    for(int i=0; i<(*m_event->m_skinPulses)->nPulses; i++){
      if((*m_event->m_skinPulses)->coincidence[i] > 2 && (*m_event->m_skinPulses)->pulseArea_phd[i] > 2.5){
        m_hists->BookFillHist("New/Skin_MS_Coin2vsA2.5", 1000, 0., 2500., 75, 0., 150., (*m_event->m_skinPulses)->pulseArea_phd[i], (*m_event->m_skinPulses)->coincidence[i], 1);
      }
    }
  }









}

// Called after event loop
void CR_vetoAnalysis::Finalize() {
  m_hists->GetHistFromMap("OD/ODPulseMS")->GetXaxis()->SetTitle("nPulsesOD");  
  m_hists->GetHistFromMap("OD/ODPulseArea")->GetXaxis()->SetTitle("pulseArea_phd");
  m_hists->GetHistFromMap("OD/ODPulseMSversion2")->GetXaxis()->SetTitle("nPulsesOD");
  m_hists->GetHistFromMap("OD/ODPulseMSversion3")->GetXaxis()->SetTitle("nPulsesOD");
  m_hists->GetHistFromMap("OD/2dHistOfAreavsPulseOD")->GetXaxis()->SetTitle("pulseArea_phd");
  m_hists->GetHistFromMap("OD/NOMSPulseOD")->GetXaxis()->SetTitle("nPulsesOD");
  m_hists->GetHistFromMap("OD/NOPulseAreaOD")->GetXaxis()->SetTitle("pulseArea_phd");
  m_hists->GetHistFromMap("OD/NO2dHistOfAreavsPulseOD")->GetXaxis()->SetTitle("pulseArea_phd");
  m_hists->GetHistFromMap("OD/2dHistOfA1vsA2")->GetXaxis()->SetTitle("A1");
  m_hists->GetHistFromMap("OD/TimeBetweenPulse1vsPulse2")->GetXaxis()->SetTitle("PeakTimePulse1");
  m_hists->GetHistFromMap("OD/V22dHistOfA1vsA2")->GetXaxis()->SetTitle("A1");
  m_hists->GetHistFromMap("Skin/SkinPulsesMS")->GetXaxis()->SetTitle("nPulsesSkin");
  m_hists->GetHistFromMap("Skin/SkinPulseArea")->GetXaxis()->SetTitle("pulseArea_phd");
  m_hists->GetHistFromMap("Skin/2dHistOfAreavsPulseSkin")->GetXaxis()->SetTitle("pulseArea_phd");
  m_hists->GetHistFromMap("Skin/NOSkinPulsesMS")->GetXaxis()->SetTitle("nPulsesSkin");
  m_hists->GetHistFromMap("Skin/NOSkinPulseArea")->GetXaxis()->SetTitle("pulseArea_phd");
  m_hists->GetHistFromMap("Skin/NO2dHistOfAreavsPulseSkin")->GetXaxis()->SetTitle("pulseArea_phd");

  m_hists->GetHistFromMap("OD/new/MS_Coin>3")->GetXaxis()->SetTitle("nPulses");
  m_hists->GetHistFromMap("OD/new/MS_Coin>3(old)")->GetXaxis()->SetTitle("nPulses");
  m_hists->GetHistFromMap("OD/new/MS_Coin>4")->GetXaxis()->SetTitle("nPulses");
  m_hists->GetHistFromMap("OD/new/MS_Coin>5")->GetXaxis()->SetTitle("nPulses");
  m_hists->GetHistFromMap("OD/new/Other_Coin>3")->GetXaxis()->SetTitle("nPulses");
  m_hists->GetHistFromMap("OD/new/Other_Coin>4")->GetXaxis()->SetTitle("nPulses");
  m_hists->GetHistFromMap("OD/new/Other_Coin>5")->GetXaxis()->SetTitle("nPulses");
  m_hists->GetHistFromMap("OD/new/Other_Pulse_Timing_for_Coin>3_PulseArea>2")->GetXaxis()->SetTitle("PulseTime(ns)");
  m_hists->GetHistFromMap("OD/new/Other_Coin5vsPulseArea4.5")->GetXaxis()->SetTitle("coincidence[i]>5");

  m_hists->GetHistFromMap("New/OD_Other_Coin5vsA4.5")->GetXaxis()->SetTitle("pulseArea_phd");
  m_hists->GetHistFromMap("New/OD_SS_Coin5vsA4.5")->GetXaxis()->SetTitle("pulseArea_phd");
  m_hists->GetHistFromMap("New/OD_PileUp_Coin5vsA4.5")->GetXaxis()->SetTitle("pulseArea_phd");
  m_hists->GetHistFromMap("New/OD_MS_Coin5vsA4.5")->GetXaxis()->SetTitle("pulseArea_phd");
  m_hists->GetHistFromMap("New/Skin_Other_Coin2vsA2.5")->GetXaxis()->SetTitle("pulseArea_phd");
  m_hists->GetHistFromMap("New/Skin_SS_Coin2vsA2.5")->GetXaxis()->SetTitle("pulseArea_phd");
  m_hists->GetHistFromMap("New/Skin_PileUp_Coin2vsA2.5")->GetXaxis()->SetTitle("pulseArea_phd");
  m_hists->GetHistFromMap("New/Skin_MS_Coin2vsA2.5")->GetXaxis()->SetTitle("pulseArea_phd");
  m_hists->GetHistFromMap("New/OD_Other_Coin5vsA4.5")->GetYaxis()->SetTitle("coincidence[i]");
  m_hists->GetHistFromMap("New/OD_SS_Coin5vsA4.5")->GetYaxis()->SetTitle("coincidence[i]");
  m_hists->GetHistFromMap("New/OD_PileUp_Coin5vsA4.5")->GetYaxis()->SetTitle("coincidence[i]");
  m_hists->GetHistFromMap("New/OD_MS_Coin5vsA4.5")->GetYaxis()->SetTitle("coincidence[i]");
  m_hists->GetHistFromMap("New/Skin_Other_Coin2vsA2.5")->GetYaxis()->SetTitle("coincidence[i]");
  m_hists->GetHistFromMap("New/Skin_SS_Coin2vsA2.5")->GetYaxis()->SetTitle("coincidence[i]");
  m_hists->GetHistFromMap("New/Skin_PileUp_Coin2vsA2.5")->GetYaxis()->SetTitle("coincidence[i]");
  m_hists->GetHistFromMap("New/Skin_MS_Coin2vsA2.5")->GetYaxis()->SetTitle("coincidence[i]");


}

